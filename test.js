let N = prompt('Введите длину генерируемой строки');
let str = "";
let dictionary = "abdefghijklmnoqrtuvwyzABDEFGHIJKLMNOQRTUVWYZ1234567890+-_$~";

for (let i = 0; i < N; i++ )
    str += dictionary.charAt(Math.floor(Math.random() * dictionary.length));

console.log(str);

let symb1 = prompt('Введите первый символ')
let count1 = 0;

const code = a => a.charCodeAt()

const setCharAt = (str, index, symb) => {
    if (index > str.length - 1) return str;
    return str.substr(0, index) + symb + str.substr(index + 1);
}

for (let i = 0; i < N; i++)
    if ((code(str[i]) >= code("A") && code(str[i]) <= code("Z")) || 
    (code(str[i]) >= code("a") && code(str[i]) <= code("z")))
    {
        str = setCharAt(str, i, symb1)
        count1++;
    }

let symb2 = prompt('Введите второй символ')
let count2 = 0;

for (let i = 0; i < N; i++)
    if (code(str[i]) >= code("0") && code(str[i]) <= code("9"))
    {
        str = setCharAt(str, i, symb2)
        count2++;
    }

console.log(str);
console.log(count1);
console.log(count2);
console.log(str.length - count1 - count2);